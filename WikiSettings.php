<?php
/**
 * General
 */

## The URL base path to the directory containing the wiki;
## defaults for all runtime URL paths are based off of this.
## For more information on customizing the URLs
## (like /w/index.php/Page_title to /wiki/Page_title) please see:
## https://www.mediawiki.org/wiki/Manual:Short_URL
$wgScriptPath = "";

## The URL path to static resources (images, scripts, etc.)
$wgResourceBasePath = $wgScriptPath;

## UPO means: this is also a user preference option

# Periodically send a pingback to https://www.mediawiki.org/ with basic data
# about this MediaWiki instance. The Wikimedia Foundation shares this data
# with MediaWiki developers to help guide future development efforts.
$wgPingback = true;

# Changing this will log out all existing sessions.
$wgAuthenticationTokenVersion = "1";

## Customizing the URLs 
#$wgArticlePath      = "$wgScript/$1";
$wgArticlePath      = "/$1";

# Case insensitive topic names
$wgCapitalLinks  = true;

# Path to the GNU diff3 utility. Used for conflict resolution.
$wgDiff3 = "/usr/bin/diff3";

## If you use ImageMagick (or any other shell command) on a
## Linux server, this will need to be set to the name of an
## available UTF-8 locale
$wgShellLocale = "en_US.utf8";

## To enable image uploads, make sure the 'images' directory
## is writable, then set this to true:
$wgEnableUploads		= true;
$wgUseImageResize		= true;
$wgUseImageMagick = true;
$wgImageMagickConvertCommand = "/usr/bin/convert";
$wgTmpDirectory = "$IP/images/temp";
$wgUploadPath       = "$wgScriptPath/upload";
$wgUploadDirectory  = "$IP/upload";

# InstantCommons allows wiki to use images from https://commons.wikimedia.org
$wgUseInstantCommons = false;

# Allow links to images on other sites. (useful for other images on arabeyes sites)
$wgAllowExternalImages = true;

#Edited by Djihed
$wgSitename         = "ويكي عربآيز";

# Site language code, should be one of the list in ./languages/data/Names.php
$wgLanguageCode = "ar";

# Use Arabic, not Indian numbers
$wgTranslateNumerals = false;

## For attaching licensing metadata to pages, and displaying an
## appropriate copyright notice / icon. GNU Free Documentation
## License and Creative Commons licenses are supported so far.
$wgRightsPage = "FreeBSD Documentation License"; # Set to the title of a wiki page that describes your license/copyright
# $wgRightsUrl = "";
# $wgRightsText = "";
# $wgRightsIcon = "";

/**
 * Performance
 */

## Uncomment this to disable output compression
# $wgDisableOutputCompression = true;

## Shared memory settings
$wgMainCacheType = CACHE_ACCEL;
$wgMessageCacheType = CACHE_ACCEL;
$wgCacheDirectory = "$IP/cache";
$wgUseLocalMessageCache = true;
$wgParserCacheType = CACHE_DB;
$wgMemCachedServers = [];
$wgSessionCacheType = CACHE_DB;
# $wgUseGzip = true;
#$wgEnableSidebarCache = true;
#$wgCompressRevisions = true; // If true, search and replaces won't work anymore

$wgJobRunRate = 0;

# Text cache
# $wgCompressRevisions = true; // use with care (see talk page)
$wgRevisionCacheExpiry = 3*24*3600;
$wgParserCacheExpireTime = 14*24*3600;

# $wgCachePages = true;
$wgUseFileCache = true; /* default: false */
$wgFileCacheDepth = 0;

# Load.php expires
// Default 30 * 24 * 60 * 60
$wgResourceLoaderMaxage['versioned']['server'] = 12 * 30 * 24 * 60 * 60;
// Default 30 * 24 * 60 * 60
$wgResourceLoaderMaxage['versioned']['client'] = 12 * 30 * 24 * 60 * 60;
// Default 30 * 24 * 60 * 60
$wgResourceLoaderMaxage['unversioned']['server'] = 12 * 30 * 24 * 60 * 60;
// Default 5 * 60
$wgResourceLoaderMaxage['unversioned']['client'] = 12 * 30 * 24 * 60 * 60;

/**
 * User administration
 */
# Users can customise their css and js. I need this to test a new look for the wiki
#$wgAllowUserJs = true;
#$wgAllowUserCss = true;

# Log IP addresses in recent list.
$wgPutIPinRC = true;

# Sysops can ban IP ranges and users.
$wgSysopRangeBans = true;

# Permissions
$wgAutoConfirmAge = 604800; /* one week */
$wgGroupPermissions['*']['edit'] = false;
$wgShowIPinHeader = false;
$wgGroupPermissions['autoconfirmed']['delete'] = true;
$wgGroupPermissions['autoconfirmed']['undelete'] = true;

# Emails
$wgEnableEmail = true;
$wgEnableUserEmail = false;
$wgEnotifUserTalk = false; # UPO
$wgEnotifWatchlist = false; # UPO
$wgEmailAuthentication = false;

/**
 * Skinning
 */
wfLoadSkin( 'CologneBlue' );
wfLoadSkin( 'Modern' );
wfLoadSkin( 'MonoBook' );
wfLoadSkin( 'Vector' );
wfLoadSkin( 'Arabeyes' );

## Default skin: you can change the default skin. Use the internal symbolic
## names, ie 'vector', 'monobook':
$wgDefaultSkin = "arabeyes";

## KuwaitNet logo
$wgFooterIcons['poweredby']['kuwaitnet'] = array(
	"src" => "$wgResourceBasePath/skins/Arabeyes/images/kuwaitnet.jpg", // you may also use a direct path to the source, e.g. "http://example.com/my/custom/path/to/MyCustomLogo.png"
	"url" => "https://kuwaitnet.net",
	"alt" => "مستضاف برعاية من كويت نت",
	// For HiDPI support, you can specify paths to larger versions of the icon.
	// "srcset" =>
	//	"/path/to/1.5x_version.png 1.5x, " .
	//	"/path/to/2x_version.png 2x",
	// If you have a non-default sized icon you can specify the size yourself.
	"height" => "50", 
	"width" => "70", 
);

## The URL path to the logo.  Make sure you change this from the default,
## or else you'll overwrite your logo when you upgrade!
# $wgLogo = "$wgResourceBasePath/resources/assets/wiki.png";
$wgLogo             = "$wgResourceBasePath/skins/Arabeyes/images/logo.png";

# Favicon
$wgFavicon = "$wgResourceBasePath/skins/common/images/favicon_arabeyes.png";

/**
 * Namespaces
 */
define("NS_TECHDICT", 100);
define("NS_TECHDICT_TALK",101);
$wgExtraNamespaces[NS_TECHDICT] = "techdict";
$wgExtraNamespaces[NS_TECHDICT_TALK] = "techdict_talk";
$wgNamespacesToBeSearchedDefault[NS_TECHDICT] = true;


define("NS_FEED", 200);
define("NS_FEED_TALK",201);
$wgExtraNamespaces[NS_FEED] = "Feed";
$wgExtraNamespaces[NS_FEED_TALK] = "Feed_talk";
$wgNamespaceProtection[NS_FEED] = array( 'editfeed' );
$wgNamespacesWithSubpages[NS_FEED] = true;
$wgGroupPermissions['sysop']['editfeed'] = true;


/**
 * Extensions
 */

# WikiEditor
wfLoadExtension( 'WikiEditor' );
$wgDefaultUserOptions['usebetatoolbar'] = 1;
$wgDefaultUserOptions['usebetatoolbar-cgd'] = 1;
$wgDefaultUserOptions['wikieditor-preview'] = 1;

# SpamBlacklist
wfLoadExtension( 'SpamBlacklist' );
$wgSpamBlacklistFiles = array( "https://meta.wikimedia.org/w/index.php?title=Spam_blacklist&action=raw&sb_ver=1", "https://en.wikipedia.org/w/index.php?title=MediaWiki:Spam-blacklist&action=raw&sb_ver=1"
);

# Inputbox
wfLoadExtension( 'InputBox' );

# News
require_once "$IP/extensions/News/News.php";
# Keep log of recent changes for one year
$wgRCMaxAge = 365 * 24 * 3600; // 365 days

# Dynamic Page List
require_once "extensions/intersection/DynamicPageList.php";

# When you make changes to this configuration file, this will make
# sure that cached pages are cleared. 
$configdate = gmdate( 'YmdHis', @filemtime( __FILE__ ) );
$wgCacheEpoch = max( $wgCacheEpoch, $configdate );

# Replace Text
wfLoadExtension( 'ReplaceText' );
$wgReplaceTextUser = "WikiBot";

# Cite
wfLoadExtension( 'Cite' );

# Parser functions
wfLoadExtension( 'ParserFunctions' );

# Echo
require_once "$IP/extensions/Echo/Echo.php";

# Thanks
wfLoadExtension( 'Thanks' );

# Nuke 
wfLoadExtension( 'Nuke' );


# Syntax highlighting
#wfLoadExtension( 'SyntaxHighlight_GeSHi' );

# Scribunto
#require_once "$IP/extensions/Scribunto/Scribunto.php";
#$wgScribuntoDefaultEngine = 'luastandalone';
#$wgScribuntoUseGeSHi = true;
#$wgScribuntoUseCodeEditor = true;

wfLoadExtension( 'UserMerge' );
// By default nobody can use this function, enable for bureaucrat?
$wgGroupPermissions['bureaucrat']['usermerge'] = true;

# 
/**
 * Transate extension
 */
$EXT = "$IP/extensions";
wfLoadExtension( 'Babel' );

wfLoadExtension( 'cldr' );

wfLoadExtension( 'LocalisationUpdate' );
$wgLocalisationUpdateDirectory = "$IP/cache";

require_once "$EXT/Translate/Translate.php";
$wgGroupPermissions['user']['translate'] = true;
$wgGroupPermissions['user']['translate-messagereview'] = true;
$wgGroupPermissions['user']['translate-groupreview'] = true;
$wgGroupPermissions['user']['translate-import'] = true;
$wgGroupPermissions['user']['pagetranslation'] = true;
$wgGroupPermissions['sysop']['translate-manage'] = true;
$wgTranslateDocumentationLanguageCode = 'qqq';
$wgExtraLanguageNames['qqq'] = 'Message documentation'; # No linguistic content. Used for documenting messages

# Added ability to change the page language for MediaWiki pages using Special:PageLanguage
$wgPageLanguageUseDB = true;
$wgGroupPermissions['user']['pagelang'] = true;

wfLoadExtension( 'UniversalLanguageSelector' );

# Localized sidebar
$wgTranslateCC['wiki-sidebar'] = 'addSidebarMessageGroup';
function addSidebarMessageGroup( $id ) {
        $mg = new WikiMessageGroup( $id, 'sidebar-messages' );
        $mg->setLabel( 'Sidebar' );
        $mg->setDescription( 'Messages used in the sidebar of this wiki' );
        return $mg;
}

# Localized mainpage
$wgTranslateCC['wiki-mainpage'] = 'addMainpageMessageGroup';
function addMainpageMessageGroup( $id ) {
        $mg = new WikiMessageGroup( $id, 'mainpage-messages' );
        $mg->setLabel( 'Mainpage' );
        $mg->setDescription( 'Messages used in the main page of this wiki' );
        return $mg;
}
# Allow titles to be changed (for main page)
$wgRestrictDisplayTitle=false;

# ULS
$wgULSLanguages=array("en" => "English", "ar" => "Arabic");
$wgULSGeoService=false;
$wgULSIMEEnabled=false;

